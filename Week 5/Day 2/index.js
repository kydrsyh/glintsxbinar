//Import Modules
const express = require('express')
const app = express()
const bodyParser = require('body-parser')

//Import routes
const transaksiRoutes = require('./routes/transaksiRoutes.js')
const pemasokRoutes = require('./routes/pemasokRoutes.js')
const barangRoutes = require('./routes/barangRoutes.js')
const pelangganRoutes = require('./routes/pelangganRoutes.js')

//Set Body Parser for HTTP post operation
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.use('/transaksi', transaksiRoutes)
app.use('/pemasok', pemasokRoutes)
app.use('/barang', barangRoutes)
app.use('/pelanggan', pelangganRoutes)

app.listen(3000)