const express = require('express')
const router = express.Router()
const TransaksiController = require('../controllers/transaksiController.js')
const TransaksiValidator = require('../middlewares/validators/transaksiValidator.js')

router.get('/', TransaksiController.getAll)
router.get('/:id', TransaksiController.getOne)
router.post('/create', TransaksiValidator.create ,TransaksiController.create)
router.put('/update/:id', TransaksiValidator.update, TransaksiController.update)
router.delete('/delete/:id', TransaksiController.delete)

module.exports = router