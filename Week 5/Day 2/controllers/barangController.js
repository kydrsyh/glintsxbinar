const client = require('../models/connection.js')
const { ObjectId } = require('mongodb')

class BarangController {
    //GetAll
    async getAll(req,res){
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')

        barang.find({}).toArray().
        then(result => {
            res.json({
                status: 'success',
                data: result
            })
        })
    }

    //GetOne
    async getOne(req,res){
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')

        barang.findOne({
            _id: new ObjectId(req.params.id)
        }).then(result =>{
            res.json({
                status: 'success',
                data: result
            })
        })
    }

    //Create
    async create(req, res){
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')
        const pemasok = await penjualan.collection('pemasok').findOne({
            _id: new ObjectId(req.body.id_pemasok)
        })

        barang.insertOne({
            nama: req.body.nama,
            harga: req.body.harga,
            pemasok: pemasok
        }).then(result =>{
            res.json({
                status: 'success',
                data: result.ops
            })
        })
    }

    //update
    async update(req, res){
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')
        const pemasok = await penjualan.collection('pemasok').findOne({
            _id: new ObjectId(req.body.id_pemasok)
        })

        barang.updateOne({
            _id: new ObjectId(req.params.id)
        },{
            $set: {
                nama: req.body.nama,
                harga: req.body.harga,
                pemasok: pemasok
            }
        }).then(result =>{
            res.json({
                status: 'success',
                data: result
            })
        })
    }

    //Delete
    async delete(req, res){
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')

        barang.deleteOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: 'success'
            })
        })
    }
}

module.exports = new BarangController