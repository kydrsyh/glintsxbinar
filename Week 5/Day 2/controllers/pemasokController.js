const client = require('../models/connection.js')
const { ObjectId } = require('mongodb')

class PemasokController {
    //GetAll
    async getAll(req,res){
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.find({}).toArray().
        then(result => {
            res.json({
                status: 'success',
                data: result
            })
        })
    }

    //GetOne
    async getOne(req,res){
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.findOne({
            _id: new ObjectId(req.params.id)
        }).then(result =>{
            res.json({
                status: 'success',
                data: result
            })
        })
    }

    //Create
    async create(req, res){
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.insertOne({
            nama: req.body.nama
        }).then(result =>{
            res.json({
                status: 'success',
                data: result.ops
            })
        })
    }

    //update
    async update(req, res){
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.updateOne({
            _id: new ObjectId(req.params.id)
        },{
            $set: {
                nama: req.body.nama
            }
        }).then(result =>{
            res.json({
                status: 'success',
                data: result
            })
        })
    }

    //Delete
    async delete(req, res){
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.deleteOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: 'success'
            })
        })
    }
}

module.exports = new PemasokController