// Import fs module
const fs = require('fs');

/* Start make promise object */
const readFile = options => file => new Promise((fulfill, reject) => {
  fs.readFile(file, options, (error, content) => {
    if (error) {
      return reject(error)
    }

    return fulfill(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* Promise object end */

/* Make options variable for fs */
const
  read = readFile('utf-8'),
  files = [
    './contents/content01.txt',
    './contents/content02.txt',
    './contents/content03.txt',
    './contents/content04.txt',
    './contents/content05.txt',
    './contents/content06.txt',
    './contents/content07.txt',
    './contents/content08.txt',
    './contents/content09.txt',
    './contents/content010.txt',
     ]

// start promise allSettled
Promise.allSettled(files.map(file => read(`${file}`)))
  .then(results => {
    // const existsContent = results.filter(result => result.status === 'fulfilled')
    // isi dari existsContent: [{ status: 'fulfilled', value: 'isi content 1' }, ...]
    // dan seterusnya kecuali content 5
    console.log(results)
  })
