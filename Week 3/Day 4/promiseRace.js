// Import fs module
const fs = require('fs');

/* Start make promise object */
const readFile = options => file => new Promise((fulfill, reject) => {
  fs.readFile(file, options, (error, content) => {
    if (error) {
      return reject(error)
    }

    return fulfill(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* Promise object end */

/* Make options variable for fs */
const read = readFile('utf-8')
/* End make options variable for fs */

/* Promise race */
Promise.race([
    read('./contents/content01.txt'),
    read('./contents/content02.txt'),
    read('./contents/content03.txt'),
    read('./contents/content04.txt'),
    read('./contents/content05.txt'),
    read('./contents/content06.txt'),
    read('./contents/content07.txt'),
    read('./contents/content08.txt'),
    read('./contents/content09.txt'),
    read('./contents/content010.txt'),
])
  .then((value) => {
    console.log(value);
  })
  .catch(error => {
    console.log(error);
  })
/* Promise race end */
