// Import fs module
const fs = require('fs');

/* Start make promise object */
const readFile = options => file => new Promise((fulfill, reject) => {
  fs.readFile(file, options, (error, content) => {
    if (error) {
      return reject(error)
    }

    return fulfill(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* Promise object end */

/* Make options variable for fs */
const read = readFile('utf-8')
/* End make options variable for fs */

/* Async function */
async function mergedContent() {
  try {
    /* This recommended */
    const result = await Promise.all([
      read('./contents/content01.txt'),
      read('./contents/content02.txt'),
      read('./contents/content03.txt'),
      read('./contents/content04.txt'),
      read('./contents/content05.txt'),
      read('./contents/content06.txt'),
      read('./contents/content07.txt'),
      read('./contents/content08.txt'),
      read('./contents/content09.txt'),
      read('./contents/content010.txt'),
    ])
    /* End this recommended */
    await writeFile('./contents/result.txt',result.join('\n'))
  } catch (error) {
    throw error
  }

  // The best practice is:
  // return promise, not return value of promise
  // not also return use await
  return read('./contents/result.txt')
}
/* Async function end */

// console.log(mergedContent()); // Promise (Pending)

// Start promise
mergedContent() // process read/write
  .then(result => {
    console.log('Success to read and write file, content:\n')// If success read/write file
    console.log(result);
  }).catch(error => {
    console.log('Failed to read/write file, content:\n', error); // If error read/write file
  }).finally(() => {
    console.log('\nPromise All is Finished!'); // run after success or error
  })
