// Make HelloController that will be used in helloRoutes
class ProfileController {

    // This function will be called in helloRoutes
    async hello(req, res) {
      try {
        console.log("You're accessing khaidarsyah!")
        res.render('khai.ejs') // if success, will be rendering html file from views/hello.ejs
      } catch (e) {
        res.status(500).send(exception) // if error, will be display "500 Internal Server Error"
      }
    }
  
  }
  
  module.exports = new ProfileController; // We don't need to instance a object bacause exporting this file will be automatically to be an object
  