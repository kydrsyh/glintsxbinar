const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

const EventEmitter = require('events')
const my = new EventEmitter()
const suspect = require('../../Week 2/Day 4/ExampleSwitchCase.js')

function loginFailed() {
    console.log("Login Failed !!");
}
my.on('Login Failed', loginFailed)

function loginSuccess() {
    console.log("\nLogin Success !!\n");
    suspect.status()
}
my.on('Login Success', loginSuccess)

function login(email, password) {
    const pass = 123456
    if (password != pass) {
        my.emit('Login Failed')
    } else {
        my.emit('Login Success')
    }
}

function question() {
    console.log(`Please insert your identity :`);
    console.log(`-----------------------------`);
    rl.question('Email: ', email => {
            rl.question('Password: ', password => {
            login(email, password)
        })
    })
}
question()
module.exports.rl = rl