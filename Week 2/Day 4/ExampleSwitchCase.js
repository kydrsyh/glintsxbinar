const login = require('../../Week 3/Day 1/Event.js')

let backendAfternoon = [{
    "name": "Khaidarsyah",
    "status": "Positive"
 },
 {
    "name": "Fahmi",
    "status": "Positive"
 },
 {
    "name": "Amanda",
    "status": "Positive"
 },
 {
    "name": "Eka",
    "status": "Positive"
 },
 {
    "name": "Heru",
    "status": "Suspect"
 },
 {
    "name": "Kevin",
    "status": "Suspect"
 },
 {
    "name": "Pudji",
    "status": "Suspect"
 },
 {
    "name": "Sahlan",
    "status": "Suspect"
 },
 {
    "name": "Sherary",
    "status": "Negative"
 },
 {
    "name": "Siti",
    "status": "Negative"
 },
 {
    "name": "Tamam",
    "status": "Negative"
 },
 {
    "name": "Wahyu",
    "status": "Negative"
 },


]

function checkStatus(option) {
    console.log(`\nThis is the ${option} in BE#2 :`);
    console.log("------------------------------");
    backendAfternoon.filter(filter => filter.status == option).
    forEach((item,i) => {console.log(`${i+1}. ${item.name}`);}) 
}

function checkYourStatus(){
    console.log("Hey this is a checker for BE#2 !!!!!");
    console.log("------------------------------------");
    console.log("1. Positive");
    console.log("2. Negative");
    console.log("3. Suspect\n");
    login.rl.question(`What is your status: `, status => {
        switch (Number(status)) {
            case 1:
                checkStatus('Positive')
                testAgain()
            break;
            case 2:
                checkStatus('Negative')
                testAgain()
            break;
            case 3:
                checkStatus('Suspect')
                testAgain()
            break;
            default:
                console.log("No no, wrong choice dude!!");
        }
    })
}

function testAgain() {
    login.rl.question(`\nCheck Again (y/n)? :`, answer => {
        if(answer == 'y') {
            console.clear()
            checkYourStatus()
        } else {
            console.log("\nThank you for using our application\n");
            login.rl.close()
        }
    })
}

// checkYourStatus()
module.exports.status = checkYourStatus