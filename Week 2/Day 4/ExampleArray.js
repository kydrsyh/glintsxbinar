let fridge = ["tomato", "broccoli", "kale", "cabbage", "apple"]

// Without using filter
for (var i = 0; i < fridge.length; i++) {
    if (fridge[i] !== 'apple') {
        console.log(`${fridge[i]} is a healthy food, it's definitely worth to eat.`)
    } else {
        console.log(`${fridge[i]} is not a vegetable\n`)
    }
}

// using Filter
function filterArray() {
    console.log("This output is using forEach");
    let result = fridge.
    filter(fridge => fridge !== "apple").
    forEach(fridge => console.log(`${fridge} is a healthy food, it's definitely worth to eat`))
}
filterArray()