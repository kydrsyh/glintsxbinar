const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

// This Function below is for calculating the cube
function typeOfGeometry () {
    console.log(`-----------------------------------------------`);
    console.log(`What kind of geometry do you want to calculate?`);
    console.log(`-----------------------------------------------`);
    console.log("1. Cube");
    console.log("2. Tube");
    console.log("3. Exit\n");
    rl.question(`Pick a number : `, geometry => {
        if(!isNaN(geometry)) {
            if(geometry == 1) {
                calculateCube()
            } else if(geometry == 2) {
                calculateTube()
            } else {
                rl.close();
            }
        } else {
            console.log("Use number to pick the options !!!!\n")
            typeOfGeometry()
        }
    } )
}

function inputLength(){
    rl.question(`length: `, length => {
        if (!isNaN(length)){
            let cubeVolume = length ** 3;
            console.log(`\nThe cube volume is ${cubeVolume} m3\n`);
            console.log(`Do you need to calculate the volume again ?`)
            rl.question(`(y/n) : `, continueProcess => {
                if(continueProcess == "y") {
                typeOfGeometry();
                } else {
                    console.log("\nThank you for using this application");
                    rl.close();
                }
        })
        } else {
            console.log("Length must be a number !!!!!\n");
            inputLength();
        }
    })
}

function calculateCube(){
    console.log("\nYou need to input the parameter for Cube");
    console.log(`-------------------------------------------`);
    inputLength();
}
// This Function above is for calculating the Cube


// This Function below is for calculating the Tube
function inputRadius(){
    rl.question(`Radius: `, radius => {
        if(!isNaN(radius)) {
            inputHeight(radius);            
        } else {
            console.log("Radius must be a number !!!!!\n");
            inputRadius();
        }
     })
}

function inputHeight(radius) {
    rl.question(`Height: `, height => {
        if(!isNaN(height)) {
            let tubeVolume = 3.14 * (radius ** 2) * height;
            console.log(`\nThe tube volume is ${tubeVolume} m3\n`);
            console.log('Do you need to calculate the volume again?')
            rl.question(`(y/n) : `, continueProcess => {
                if(continueProcess == "y") {
                typeOfGeometry();
                } else {
                    console.log("\nThank you for using this application");
                    rl.close();
                }
            })
        } else {
            console.log("Height must be a number !!!!!\n");
            inputHeight(radius);
         }
    })
}

function calculateTube(){
    console.log("\nYou need to input the parameter for Tube")
    console.log(`-------------------------------------------`);
    inputRadius();
}

//This calling the function
typeOfGeometry();