const {Pelanggan} = require("../../models")
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

module.exports = {
  create: [
    //Set form validation rule
    check('nama').isLength({ min: 1 }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  update: [
    check('nama').isLength({ min: 1 }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  delete: [
    //Set form validation rule
    check('id').isLength({ min: 1 }).isNumeric().custom(value => {
      return Pelanggan.findOne({
        where: {
          id: value
        }
      }).then(b => {
        if (!b) {
          throw new Error('ID Pelanggan not found');
        }
      })
    }),
  ]
};
