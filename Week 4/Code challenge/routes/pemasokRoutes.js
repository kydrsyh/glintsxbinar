const express = require('express') // Import express
const router = express.Router() // Make router from app
const pemasokController = require('../controllers/pemasokController.js') // Import TransaksiController
const pemasokValidator = require('../middlewares/validators/pemasokValidator.js') // Import validator to validate every request from user

router.get('/', pemasokController.getAll) // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get('/:id', pemasokController.getOne) // If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.post('/create', pemasokValidator.create, pemasokController.create) // If accessing localhost:3000/transaksi/create, it will call create function in TransaksiController class
router.put('/update/:id', pemasokValidator.update, pemasokController.update) // If accessing localhost:3000/transaksi/update/:id, it will call update function in TransaksiController class
router.delete('/delete/:id', pemasokValidator.delete, pemasokController.delete) // If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class

module.exports = router; // Export router
