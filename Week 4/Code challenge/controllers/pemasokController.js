const {Pemasok } = require("../models")

class PemasokController {

// Get All data from transaksi
async getAll(req, res) {
    Pemasok.findAll({ // find all data of Transaksi table
      attributes: ['id', 'nama',['createdAt', 'waktu']], // just these attributes that showed
    }).then(pemasok => {
      res.json(pemasok) // Send response JSON and get all of Transaksi table
    })
  }
// Get One data from transaksi
async getOne(req, res) {
    Pemasok.findOne({ // find one data of Transaksi table
      where: {
        id: req.params.id // where id of Transaksi table is equal to req.params.id
      },
      attributes: ['id', 'nama',['createdAt', 'waktu']], // just these attributes that showed
    }).then(pemasok => {
      res.json(pemasok) // Send response JSON and get one of Transaksi table depend on req.params.id
    })
  }
// Create Transaksi data
async create(req, res) {
    Pemasok.create({
        nama: req.body.nama
    }).then(newPemasok => {
        res.json({
            "status": "success",
            "message": "Pemasok created",
            "data": newPemasok
        })
    })
}

// Update Transaksi data
async update(req, res) {
      // Make update query
      var update = {
        nama: req.body.nama
      }
      // Transaksi table update data
      return Pemasok.update(update, {
        where: {
          id: req.params.id
        }
      })
        .then(affectedRow => {
      return Pemasok.findOne({ // find one data of Transaksi table
        where: {
          id: req.params.id  // where id of Transaksi table is equal to req.params.id
        }
      })
    }).then(b => {
      // Send response JSON and get one of Transaksi table that we've updated
      res.json({
        "status": "success",
        "message": "Pemasok updated",
        "data": b
      })
    })
  }
// Soft delete Transaksi data
async delete(req, res) {
    Pemasok.destroy({  // Delete data from Transaksi table
      where: {
        id: req.params.id  // Where id of Transaksi table is equal to req.params.id
      }
    }).then(affectedRow => {
      // If delete success, it will return this JSON
      if (affectedRow) {
        return {
          "status": "success",
          "message": "Pemasok deleted",
          "data": null
        }
      }

      // If failed, it will return this JSON
      return {
        "status": "error",
        "message": "Failed",
        "data": null
      }
    }).then(r => {
      res.json(r) // Send response JSON depends on failed or success
    })
  }
}

module.exports = new PemasokController;
