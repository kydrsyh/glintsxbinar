const express = require('express')
const router = express.Router()
const passport = require('passport')
const auth = require('../middlewares/auth')
const transaksiValidator = require('../middlewares/validators/transaksiValidator')
const TransaksiController = require('../controllers/transaksiController')

router.get('/', [passport.authenticate('transaksi', { session: false })], TransaksiController.getAll)
router.get('/:id',transaksiValidator.getOne, TransaksiController.getOne)
router.post('/create', transaksiValidator.create, TransaksiController.create)
router.put('/update/:id', transaksiValidator.update, TransaksiController.update)
router.delete('/delete/:id', transaksiValidator.delete, TransaksiController.delete)

module.exports = router