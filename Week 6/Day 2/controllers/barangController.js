const {
    barang,
    pemasok
} = require('../models/mongodb')

class BarangController {

    async getAll(req, res) {
        barang.find({})
            .then(result => {
                res.json({
                    status: "success",
                    data: result
                })
            })
    }

    async getOne(req, res) {
        barang.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        const data = await pemasok.findOne({
                _id: req.body.id_pemasok
            })

        barang.create({
            nama: req.body.nama,
            harga: req.body.harga,
            pemasok: data,
            image: req.file === undefined ? "" : req.file.path,
        }).then(result => {
            res.json({
                status: 'success',
                data: result
            })
        })
    }

    async update(req, res) {
        const data = await pemasok.findOne({
            _id: req.body.id_pemasok
        })

        barang.findOneAndUpdate({
            _id: req.params.id
        }, {
            nama: req.body.nama,
            harga: req.body.harga,
            pemasok: data,
            image: req.file === undefined ? "" : req.file.path
        },{new: true}).then(result => {
            res.json({
                status: 'success',
                data: result
            })
        })
    }

    async delete (req, res) {
        barang.delete({
            _id: req.params.id
        }).then(() => {
            res.json({
                status: 'success',
                data: null
            })
        })
    }
}

module.exports = new BarangController