const {user} = require('../models/mysql')
const passport = require('passport')
const jwt = require('jsonwebtoken')

class UserController {
    async signup(user, req, res) {
        const body = {
            id: user.dataValues.id,
            email: user.dataValues.email
        }
        console.log(body);
        const token = await jwt.sign({
            user: body
        }, 'secret_password')

        res.status(200).json({
            message: 'Signup success',
            token: token
        })
    }
    async login(user,req, res) {
        // get the req.user from passport authentication
        const body = {
          id: user.dataValues.id,
          email: user.dataValues.email
        };
    
        // create jwt token from body variable
        const token = jwt.sign({
          user: body
        }, 'secret_password')
    
        // success to create token
        res.status(200).json({
          message: 'Login success!',
          token: token
        })
      }
    
}

module.exports = new UserController