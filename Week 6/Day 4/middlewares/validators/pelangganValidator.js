const {
    pelanggan
} = require('../../models')
const {
    check,
    validationResult
} = require('express-validator')

module.exports = {
    getOne: [
        check('id').custom(value => {
            return pelanggan.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('Id was not found')
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    create: [
        check('nama').isString().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    update: [
        check('id').custom(value => {
            return pelanggan.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error("Id Pelanggan was not found")
                }
            })
        }),
        check('nama').isString().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    delete: [
        check('id').custom(value => {
            return pelanggan.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Id Pelanggan was not found')
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
}