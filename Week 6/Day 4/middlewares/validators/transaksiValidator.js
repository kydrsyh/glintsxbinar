const {
    barang,
    pelanggan,
    transaksi
} = require('../../models')
const {
    check,
    validationResult
} = require('express-validator')

module.exports = {
    getOne: [
        check('id').custom(value => {
            return transaksi.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('Id was not found')
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    create: [
        check('id_barang').custom(value => {
            return barang.findOne({
                _id: value
            }).then( result => {
                if(!result) {
                    throw new Error('Id Barang was not found')
                }
            })
        }),
        check('id_pelanggan').custom(value => {
            return pelanggan.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Id Pelanggan was not found')
                }
            })
        }),
        check('jumlah').isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    update: [
        check('id').custom(value => {
            return transaksi.findOne({
                _id:value
            }).then(result => {
                if(!result) {
                    throw new Error ('Id Transaksi was not found')
                }
            })
        }),
        check('id_barang').custom(value => {
            return barang.findOne({
                _id: value
            }).then( result => {
                if(!result) {
                    throw new Error('Id Barang was not found')
                }
            })
        }),
        check('id_pelanggan').custom(value => {
            return pelanggan.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Id Pelanggan was not found')
                }
            })
        }),
        check('jumlah').isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    delete: [
        check('id').custom(value => {
            return transaksi.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Id Transaksi was not found')
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
}